from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.registerPage, name="registerPage"),
    path('login/', views.loginPage, name="loginPage"),
    path('logout', views.logoutPage, name='logoutPage')
]

